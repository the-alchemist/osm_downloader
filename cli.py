#!/usr/bin/python3
import sys

import click

from modules.downloader import OsmDownloader


@click.command()
@click.option("--cities", is_flag=True, help="Load cities from OSM")
@click.option("--countries", is_flag=True, help="Load countries from OSM")
@click.option("--regions", is_flag=True, help="Load regions from OSM")
@click.option("--start-from", help="Load from range of defined country code")
@click.option("--to", help="Load to range of defined country code")
@click.option("--only", help="Load only one defined country")
@click.option("--admin-level", default=2, help="Load cities only to selected admin level")
@click.option("--clean-points", is_flag=True, help="Clearing all points from already loaded json")
def main(cities=None, countries=None, regions=None, startFrom=None, endTo=None, only=None, adminLevel=None,
         clearPoints=None):
    downloader = OsmDownloader()
    if cities:
        downloader.DownloadCities()
        if adminLevel:
            downloader.DownloadCities(adminLevel)
    elif countries:
        print("Loading countries: ", end="")
        if startFrom:
            print("from %s " % startFrom, end="")
            downloader.DownloadCountries(countryFrom=startFrom)
        elif endTo:
            print("to %s" % endTo)
            if startFrom:
                downloader.DownloadCountries(countryFrom=startFrom, countryTo=endTo)
            else:
                downloader.DownloadCountries(countryTo=endTo)
        elif only:
            print("only %s" % only)
            downloader.DownloadCountries(oneCountryOnly=only)
        else:
            print("all")
            downloader.DownloadCountries()
        print("\n")
    elif regions:
        downloader.DownloadRegions()
    if clearPoints:
        clearPoints()


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("\nOSM Downloader: \n\ttype --help for list of all commands\n")
        sys.exit()
    main()
