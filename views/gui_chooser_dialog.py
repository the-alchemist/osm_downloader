#!/usr/bin/python3
import json
import string
from functools import partial

from PyQt5.QtCore import pyqtSlot, QUrl, Qt
from PyQt5.QtWebEngineWidgets import QWebEngineView
from PyQt5.QtWidgets import *
from PyQt5.QtWinExtras import QWinTaskbarProgress
from PyQt5.uic import loadUi

import bs4
import requests


class OsmChooserDialog(QDialog):

    def __ui_stubs(self):
        self.chooserForm = QWidget()
        self.tabWidget = QTabWidget()
        raise AssertionError("This should never be called")

    # QLabel mylabel;
    # mylabel.setPixmap("myFile.png");
    # mylabel.show();

    '''
    QImage *image = new QImage(view->page()->mainFrame()->contentsSize(), QImage::Format_ARGB32);
        QPainter *painter = new QPainter(image);
    
    view->page()->mainFrame()->render(painter);
    
    painter->end();
    image->save(view->title() + "png");
    '''

    polygons = []

    def setPolygons(self, polygons):

        self.polygonsDict = json.loads(polygons)


        self.polygons = polygons

    @pyqtSlot()
    def onPolygonSelected(self, polygon):
        successMessageBox = QMessageBox()
        successMessageBox.setIcon(QMessageBox.Information)
        successMessageBox.setText(f"Selected polygon: {polygon.get('id')} ({polygon.get('title')})")
        successMessageBox.setWindowTitle("Success!")
        successMessageBox.setStandardButtons(QMessageBox.Ok)
        successMessageBox.exec_()
        self.close()

    @pyqtSlot()
    def onMapLoaded(self, progressBar, webView, button):
        # webView.page().runJavaScript("$('#sidebar_content').find('h2').first().text()", self.polygonTitle)
        webView.page().runJavaScript("$('#content').children(':not(#map)').remove();$('header').remove();$('.leaflet-control-container').remove();$('.leaflet-contextmenu').remove();$('#content').css('top','0');")
        progressBar.hide()
        webView.show()
        button.show()

    def renderWebPolygon(self, polygon):
        url = f"https://www.openstreetmap.org/relation/{polygon.get('id')}/#map=8"

        webView = QWebEngineView()
        webView.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        # webView.setContentsMargins(0, 0, 0, 0)
        webView.setUrl(QUrl(url))
        webView.hide()

        button = QCommandLinkButton()
        button.setText(f"Select polygon #{polygon.get('id')}")
        button.setDescription(polygon.get('title'))
        button.hide()

        progressBar = QProgressBar()
        progressBar.setAlignment(Qt.AlignCenter)

        webView.loadProgress.connect(progressBar.setValue)

        webView.loadFinished.connect(partial(self.onMapLoaded, progressBar, webView, button))

        button.clicked.connect(partial(self.onPolygonSelected, polygon))

        # label = QLabel()
        # label.setText("To zoom, hover on map and use mousewheel")

        widget = QWidget()
        gridLayout = QGridLayout()
        gridLayout.setSpacing(0)
        # gridLayout.setContentsMargins(0, 0, 0, 0)
        # gridLayout.addWidget(label)
        gridLayout.addWidget(progressBar)
        gridLayout.addWidget(webView)
        gridLayout.addWidget(button)
        widget.setLayout(gridLayout)

        self.tabWidget.addTab(widget, f"Polygon #{polygon.get('id')} ({polygon.get('title')})")
        # self.tabWidget.setContentsMargins(0, 0, 0, 0)

        # self.scrollArea.setLayout(verticalLayout)

    def __setComponents(self):
        self.setWindowTitle("Choose one of preferred polygons")
        self.tabWidget.setUsesScrollButtons(False)
        # self.polygons = [
        #     {"id": 51490, "title": "Moscow Oblast"},
        #     {"id": 102269, "title": "Moscow"},
        #     {"id": 1029256, "title": "Central Federal District"},
        #     {"id": 2555133, "title": "Moscow"}
        # ]
        for polygon in self.polygons:
            self.renderWebPolygon(polygon)

    def __init__(self, parent=None):
        super(OsmChooserDialog, self).__init__(parent)
        loadUi("./ui/osm_chooser_dialog.ui", self)
        self.__setComponents()
        # self.__setConnections()
