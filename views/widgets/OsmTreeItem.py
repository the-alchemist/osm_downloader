from PyQt5.QtCore import Qt
from PyQt5.QtCore import pyqtSlot, pyqtSignal
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *


class OsmTreeItem(QTreeWidgetItem):

    downloadCompleted = pyqtSignal()
    itemChecked = pyqtSignal()

    @property
    def title(self):
        return self.text(0)

    @property
    def status(self):
        return self.itemStatusLabel.text()

    def __init__(self, parent, text):
        super(OsmTreeItem, self).__init__(parent)

        # Column 0 - Text
        self.setText(0, text)

        # Column 1 - Status
        self.itemStatusLabel = QLabel()
        self.itemStatusLabel.setText("not loaded")
        self.itemStatusLabel.setAlignment(Qt.AlignCenter)
        self.treeWidget().setItemWidget(self, 1, self.itemStatusLabel)

        # Column 2 - ProgressBar
        # self.itemDownloadingProgress = QProgressBar()
        # # self.itemDownloadingProgress.setFixedWidth(100)
        # self.itemDownloadingProgress.setAlignment(Qt.AlignHCenter)
        # self.itemDownloadingProgress.setValue(0)
        # self.treeWidget().setItemWidget(self, 2, self.itemDownloadingProgress)

    def setStatusDownloaded(self):
        self.itemStatusLabel.setText("Loaded")
        self.itemStatusLabel.setStyleSheet('''
            background-color: rgb(0, 255, 0);
            color: rgb(255, 255, 255);
        ''')

    def setStatusInProgress(self):
        self.itemStatusLabel.setText("Loading")
        self.itemStatusLabel.setStyleSheet('''
            color: rgb(255, 255, 0);
        ''')

    def setStatusDefault(self):
        self.itemStatusLabel.setText("Not loaded")
        self.itemStatusLabel.setStyleSheet('''
            color: rgb(255, 0, 0);
        ''')
