#!/usr/bin/python3
import json
import os
import subprocess
from functools import partial

from PyQt5.QtCore import *
from PyQt5.QtGui import QTextCursor
from PyQt5.QtWidgets import *
from PyQt5.uic import loadUi

from modules.downloader import OsmDownloader
from modules.workers.worker import Worker
from views.gui_chooser_dialog import OsmChooserDialog
from views.gui_settings_dialog import OsmSettingsDialog
from views.widgets.OsmTreeItem import OsmTreeItem


class OsmDownloaderWindow(QMainWindow):

    def __ui_stubs(self):
        self.MainWindow = QMainWindow()
        self.centralwidget = QWidget()
        self.groupBox_OsmData = QGroupBox()
        self.lineEdit_SearchField = QLineEdit()
        self.treeWidget_OsmStructure = QTreeWidget()
        self.groupBox_LoadSettings = QGroupBox()
        self.label_InFolder = QLabel()
        self.lineEdit_DataFolderPath = QLineEdit()
        self.toolButton_OpenFolder = QToolButton()
        self.toolButton_ImportCitiesFromFile = QToolButton()
        self.checkBox_SaveLogs = QCheckBox()
        self.toolBox = QToolBox()
        self.pageCountries = QWidget()
        self.layoutWidget = QWidget()
        self.checkBox_LoadTo = QCheckBox()
        self.checkBox_LoadOnly = QCheckBox()
        self.lineEdit_CountryTo = QLineEdit()
        self.checkBox_LoadSpecificCountries = QCheckBox()
        self.plainTextEdit_CountriesForSpecificCountriesField = QPlainTextEdit()
        self.checkBox_LoadFrom = QCheckBox()
        self.lineEdit_CountryFrom = QLineEdit()
        self.pushButton_LoadCountries = QPushButton()
        self.lineEdit_CountryOnly = QLineEdit()
        self.pageRegions = QWidget()
        self.layoutWidget = QWidget()
        self.label_RegionAdminLevel = QLabel()
        self.comboBox_RegionsAdminLevel = QComboBox()
        self.checkBox_LoadAllRegionsLevels = QCheckBox()
        self.checkBox_LoadRegionsForSpecificCountries = QCheckBox()
        self.pushButton_LoadRegions = QPushButton()
        self.plainTextEdit_SpecificRegionCountriesField = QPlainTextEdit()
        self.pageCities = QWidget()
        self.layoutWidget = QWidget()
        self.checkBox_LoadAllCitiesLevels = QCheckBox()
        self.radioButton_LoadCitiesAsPoints = QRadioButton()
        self.comboBox_CitiesAdminLevel = QComboBox()
        self.label_CitiesAdminLevel = QLabel()
        self.plainTextEdit_CitiesForSpecificCountriesField = QPlainTextEdit()
        self.radioButton_LoadCitiesWithPolygons = QRadioButton()
        self.pushButton_LoadCities = QPushButton()
        self.checkBox_LoadCitiesForSpecificCountries = QCheckBox()
        self.groupBox_Status = QGroupBox()
        self.listWidget_StatusLog = QListWidget()
        self.groupBox_Progress = QGroupBox()
        self.progressBar_DataLoadingProgress = QProgressBar()
        self.menuBar = QMenuBar()
        self.menuFile = QMenu()
        self.menuSettings = QMenu()
        self.statusBar = QStatusBar()
        raise AssertionError("This should never be called")

    def activateProgressBar(self, title):
        self.groupBox_Progress.show()
        self.groupBox_Progress.setTitle(title)
        self.progressBar_DataLoadingProgress.setValue(0)

    def deactivateProgressBar(self):
        self.progressBar_DataLoadingProgress.setValue(0)
        self.groupBox_Progress.hide()

    def toggleSpecificCitiesEnvironment(self, enabled):
        self.plainTextEdit_CitiesForSpecificCountriesField.setEnabled(enabled)
        self.toolButton_ImportCitiesFromFile.setEnabled(enabled)
        self.label_CitiesAdminLevel.setDisabled(enabled)
        self.comboBox_CitiesAdminLevel.setDisabled(enabled)
        self.checkBox_LoadAllCitiesLevels.setDisabled(enabled)

    def addTreeItem(self, title, parent, items=None, checkable=False):
        treeItem = OsmTreeItem(parent, f"{title}")
        if checkable:
            treeItem.setFlags(treeItem.flags() | Qt.ItemIsUserCheckable)
        if items:
            for item in items:
                newChild = OsmTreeItem(treeItem, item)
                newChild.setStatusDownloaded()
                treeItem.addChild(newChild)
        if type(parent) is QTreeWidgetItem:
            treeItem.setStatusDownloaded()
            parent.addChild(treeItem)
        # else:
        #     parent.addTopLevelItem(newItem)
        return treeItem

    def initAdminLevelComboBox(self):
        adminLevels = [
            "[0] - Continent",
            "[1] - Empire",
            "[2] - Country",
            "[3] - MacroRegion",
            "[4] - Region",
            "[5] - Macrocounty",
            "[6] - County",
            "[7] - MetroArea",
            "[8] - Locality",
            "[9] - Macrohood",
            "[10] - Neighbourhood",
            "[11] - Microhood",
            "[12] - Venue",
            "[13] - Island"
        ]
        self.comboBox_RegionsAdminLevel.addItems(adminLevels)
        self.comboBox_CitiesAdminLevel.addItems(adminLevels)

        self.comboBox_RegionsAdminLevel.setCurrentIndex(4)
        self.comboBox_CitiesAdminLevel.setCurrentIndex(2)

    def initOsmStructureTree(self):

        items = []

        currentStructure = {
            "countries": [],
            "regions": {
                "County": [],
                "Locality": [],
                "Region": []
            },
            "cities": []
        }
        tree = self.treeWidget_OsmStructure
        tree.setContextMenuPolicy(Qt.CustomContextMenu)
        tree.customContextMenuRequested.connect(self.openOsmTreeMenu)
        treeHeaders = ("Object", "Status")
        tree.setColumnCount(len(treeHeaders))
        tree.setColumnWidth(0, 300)
        tree.setHeaderLabels(treeHeaders)
        tree.setHeaderHidden(False)
        tree.setExpandsOnDoubleClick(False)
        # tree.hideColumn(2)

        for root, directories, filenames in os.walk(self.settings["path_osm_data"]):
            for index, filename in enumerate(filenames):
                items.append(
                    {
                        "type": root.rsplit('\\', 1)[1],
                        "path": f"{root}\\{filename}",
                        "filename": filename
                    }
                )

        for item in items:
            if "Country" in item["type"]:
                currentStructure["countries"].append(item["filename"])
            if "County" in item["type"]:
                currentStructure["regions"]["County"].append(item["filename"])
            if "Locality" in item["type"]:
                currentStructure["regions"]["Locality"].append(item["filename"])
            if "Region" in item["type"]:
                currentStructure["regions"]["Region"].append(item["filename"])
            if "City" in item["type"]:
                currentStructure["cities"].append(item["filename"])

        # Headers
        countriesHeader = self.addTreeItem(f"Countries ({len(currentStructure['countries'])})", tree, currentStructure["countries"])
        regionsHeader = self.addTreeItem(f"Administrative regions ({len(currentStructure['regions'])})", tree)
        citiesHeader = self.addTreeItem(f"Cities ({len(currentStructure['cities'])})", tree, currentStructure["cities"])

        # Sub-groups
        self.addTreeItem(f"Counties ({len(currentStructure['regions']['County'])})", regionsHeader, currentStructure["regions"]["County"], True)
        self.addTreeItem(f"Localities ({len(currentStructure['regions']['Locality'])})", regionsHeader, currentStructure["regions"]["Locality"], True)
        self.addTreeItem(f"Regions ({len(currentStructure['regions']['Region'])})", regionsHeader, currentStructure["regions"]["Region"], True)

        # for column in range(tree.columnCount()):
        #     tree.resizeColumnToContents(column)

        self.treeWidget_OsmStructure.itemDoubleClicked.connect(self.onOsmTreeItemChecked)

    def openOsmTreeMenu(self, position):

        self.treeMenu = QMenu()

        self.actionOpenInExplorer = QAction("Open in Explorer")
        self.treeMenu.addAction(self.actionOpenInExplorer)
        self.actionOpenInExplorer.triggered.connect(self.openInExplorer)

        self.treeMenu.addAction("Clear")
        self.treeMenu.addAction("Refresh")

        self.treeMenu.exec_(self.treeWidget_OsmStructure.viewport().mapToGlobal(position))

    def openInExplorer(self, path):

        pathStructure = {
            "Countries": "OSM_COUNTRIES",
            "Cities": "OSM_CITIES",
            "regions": "OSM_REGIONS",
            "localities": "OSM_REGIONS\\Locality",
            "counties": "OSM_REGIONS\\County",
            "macroregions": "OSM_REGIONS\\MacroRegion",
        }

        text = self.treeWidget_OsmStructure.selectedIndexes()[0].data(Qt.DisplayRole)  # or ix.data()

        path = f'''{self.settings["path_osm_data"]}\\[{pathStructure[text].split(" ")[0]}]\\'''
        os.system(f'''explorer {path}''')

    def toggleOsmTreeItem(self, item):
        if Qt.ItemIsUserCheckable not in item.flags():
            item.setFlags(item.flags(), Qt.ItemIsUserCheckable)
        if item.checkState is Qt.Checked:
            item.setCheckState(0, Qt.Unchecked)
        if item.checkState is Qt.Unchecked:
            item.setCheckState(0, Qt.Checked)

    def onOsmTreeItemChecked(self, item):
        fileNameWithoutExtension = item.title.split('.')[0]
        extension = item.title.rsplit('.', 1)[1]
        adminLevel = fileNameWithoutExtension.rsplit('_', 1)[1]
        folder = f"{adminLevel}\\"
        if "City" in item.parent().title:
            folder = f"{adminLevel}\\City"
        path = f"{self.settings['path_osm_data']}\\{folder}\\{item.title}"
        if ".json" in path:
            QMessageBox.information(self, "FFF", f"Selected item {item.title} \n @ \n {path}")
            os.system(f'code "{path}"')
        # item.setFlags(item.flags() | Qt.ItemIsUserCheckable)
        # item.setCheckState(0, Qt.Checked)
        # self.toggleItem(item)

    def onOsmTreeDataSearch(self, text):
        foundItems = self.treeWidget_OsmStructure.findItems(text, Qt.MatchContains | Qt.MatchRecursive, 0)
        self.groupBox_OsmData.setTitle(f"Searching \"{text}\", found {len(foundItems)} items")
        # self.treeWidget_OsmStructure.selectionModel.select(found_items)
        for item in foundItems:
            self.treeWidget_OsmStructure.setCurrentItem(item)
            # self.treeWidget_OsmStructure.selectionModel.select(item)
            # self.treeWidget_OsmStructure.setSelectionMode(item.MultiSelection)
        QCoreApplication.processEvents()

    def error(self):
        QMessageBox.warning(self, "Error!", "Error loading data!")

    def done(self):
        # QMessageBox.information(self, "Done!", "Done downloading data!")
        self.progressBar_DataLoadingProgress.setValue(self.progressBar_DataLoadingProgress.value() + 1)

    def writerLog(self, text):
        self.listWidget_StatusLog.addItem(QListWidgetItem(f"[writer] {text}"))
        self.listWidget_StatusLog.scrollToBottom()

    def log(self, text):
        self.listWidget_StatusLog.addItem(QListWidgetItem(text))
        self.listWidget_StatusLog.scrollToBottom()

    def openChooser(self, cities):
        chooserDialog = OsmChooserDialog(self)
        chooserDialog.setPolygons(cities)
        chooserDialog.show()

    def __DownloadCountries(self, progress_callback):
        loadSpecificCountries = self.checkBox_LoadSpecificCountries.isChecked()
        loadFrom = self.checkBox_LoadFrom.isChecked()
        loadTo = self.checkBox_LoadTo.isChecked()
        loadOnly = self.checkBox_LoadOnly.isChecked()
        loadCountryFrom = ""
        loadCountryTo = ""
        loadCountryOnly = ""
        if loadSpecificCountries:
            specificCountries = self.plainTextEdit_CountriesForSpecificCountriesField.toPlainText().upper().replace("\n", "").split(",")
        if loadFrom:
            loadCountryFrom = self.lineEdit_CountryFrom.text().upper()
        if loadTo:
            loadCountryTo = self.lineEdit_CountryTo.text().upper()
        if loadOnly:
            loadCountryOnly = self.lineEdit_CountryOnly.text().upper()
        self.log("Starting to download countries.\nTo cancel operation, just close app.")
        self.downloader.DownloadCountries(countryFrom=loadCountryFrom, countryTo=loadCountryTo, oneCountryOnly=loadCountryOnly)

    def __DownloadCities(self, progress_callback):

        adminLevel = self.comboBox_CitiesAdminLevel.currentIndex()
        listAllLevels = self.checkBox_LoadAllCitiesLevels.isChecked()
        loadAsPoints = self.radioButton_LoadCitiesAsPoints.isChecked()

        specificCities = []

        specificCitiesText = self.plainTextEdit_CitiesForSpecificCountriesField.toPlainText()
        specificCities = specificCitiesText.split('\n')

        self.log('Starting to download cities.\nTo cancel operation, just close app.')

        self.downloader.DownloadCities(adminLevel, loadAsPoints, listAllLevels, specificCities)

    def __DownloadRegions(self, progress_callback):

        loadSpecificCountries = self.checkBox_LoadRegionsForSpecificCountries.isChecked()
        deepestAdminLevel = self.comboBox_RegionsAdminLevel.currentIndex()
        specificCountries = []
        if loadSpecificCountries:
            specificCountries = self.plainTextEdit_SpecificRegionCountriesField.toPlainText().upper().replace("\n", "").split(",")
            self.log("Starting to download regions.\nTo cancel operation, just close app.")
            self.downloader.DownloadRegions(deepestAdminLevel=deepestAdminLevel, specifiedCountries=specificCountries)
        else:
            self.log("Starting to download regions.\nTo cancel operation, just close app.")
            self.downloader.DownloadRegions(deepestAdminLevel=deepestAdminLevel)

        print(f"Downloading regions specific:{loadSpecificCountries}, countries:{specificCountries}, deepest:{deepestAdminLevel}")

    def onLoadCities(self, checked):

        self.downloadCitiesWorker.signals.result.connect(self.log)
        self.downloadCitiesWorker.signals.error.connect(self.error)
        self.downloadCitiesWorker.signals.finished.connect(self.done)
        self.downloadCitiesWorker.signals.progress.connect(self.progressBar_DataLoadingProgress.setValue)

        if checked:
            self.pushButton_LoadCities.setText("Stop")
            self.treeWidget_OsmStructure.showColumn(2)
            self.activateProgressBar("Downloading cities...")
            self.downloadCitiesWorker.start()
        else:
            self.pushButton_LoadCities.setText("Start")
            self.downloadCitiesWorker.stop()
            self.deactivateProgressBar()

    def onLoadRegions(self, checked):

        self.downloadRegionsWorker.signals.result.connect(self.log)
        self.downloadRegionsWorker.signals.error.connect(self.error)
        self.downloadRegionsWorker.signals.finished.connect(self.done)
        self.downloadRegionsWorker.signals.progress.connect(self.progressBar_DataLoadingProgress.setValue)

        if checked:
            self.activateProgressBar("Downloading regions...")
            self.pushButton_LoadRegions.setText("Stop")
            self.treeWidget_OsmStructure.showColumn(2)
            self.downloadRegionsWorker.start()
        else:
            self.pushButton_LoadRegions.setText("Start")
            self.downloadRegionsWorker.stop()
            self.deactivateProgressBar()

    def onLoadCountries(self, checked):

        self.downloadCountriesWorker.signals.result.connect(self.log)
        self.downloadCountriesWorker.signals.error.connect(self.error)
        self.downloadCountriesWorker.signals.finished.connect(self.done)
        self.downloadCountriesWorker.signals.progress.connect(self.progressBar_DataLoadingProgress.setValue)

        if checked:
            self.activateProgressBar("Downloading countries...")
            # self.appendStatusLog("Downloading countries")
            self.pushButton_LoadCountries.setText("Stop")
            self.treeWidget_OsmStructure.showColumn(2)
            self.downloadCountriesWorker.start()
        else:
            self.pushButton_LoadCountries.setText("Start")
            self.downloadCountriesWorker.stop()
            self.deactivateProgressBar()

    def onImportCitiesFile(self):
        dialog = QFileDialog()
        filePath = dialog.getOpenFileName(self, 'Select file with cities', filter="All files (*.*);;Text files (*.txt);;XML files (*.xml);;JSON files (*.json);;CSV files (*.csv)")
        if filePath:
            self.plainTextEdit_CitiesForSpecificCountriesField.clear()
            fileText = open(filePath[0]).read()
            fileTextFormatted = fileText.replace('|', ' - ').title()
            fileTextPurified = list(dict.fromkeys(fileTextFormatted.split("\n")))
            for index, line in enumerate(fileTextPurified):
                if line.count("-") > 1:
                    fileTextPurified[index] = line.rsplit('-', 1)[0].strip()
            self.plainTextEdit_CitiesForSpecificCountriesField.insertPlainText("\n".join(fileTextPurified).strip("-"))

    def onOpenSettingsDialog(self):
        dialog = OsmSettingsDialog(self)
        dialog.show()

    def __setUIConnections(self):
        # toolButton connection
        self.toolButton_ImportCitiesFromFile.clicked.connect(self.onImportCitiesFile)

        # Load data button connections
        self.pushButton_LoadCities.clicked.connect(self.onLoadCities)
        self.pushButton_LoadRegions.clicked.connect(self.onLoadRegions)
        self.pushButton_LoadCountries.clicked.connect(self.onLoadCountries)

        # Checkboxes connections
        self.checkBox_LoadRegionsForSpecificCountries.clicked[bool].connect(self.plainTextEdit_SpecificRegionCountriesField.setEnabled)
        self.checkBox_LoadSpecificCountries.clicked[bool].connect(self.plainTextEdit_CountriesForSpecificCountriesField.setEnabled)
        self.checkBox_LoadCitiesForSpecificCountries.clicked[bool].connect(self.toggleSpecificCitiesEnvironment)

        # Special checkboxes connections
        self.checkBox_LoadFrom.clicked[bool].connect(self.lineEdit_CountryFrom.setEnabled)
        self.checkBox_LoadOnly.clicked[bool].connect(self.lineEdit_CountryOnly.setEnabled)
        self.checkBox_LoadTo.clicked[bool].connect(self.lineEdit_CountryTo.setEnabled)

        self.checkBox_LoadAllCitiesLevels.clicked[bool].connect(self.comboBox_CitiesAdminLevel.setDisabled)
        self.checkBox_LoadAllRegionsLevels.clicked[bool].connect(self.comboBox_RegionsAdminLevel.setDisabled)

        # Search field connection
        self.lineEdit_SearchField.textChanged[str].connect(self.onOsmTreeDataSearch)

        # On open settings
        self.action_OpenSettings.triggered.connect(self.onOpenSettingsDialog)
        # self.actionOpenInExplorer.triggered.connect(self.openInExplorer)
        # self.actionExit.triggered.connect(self.openChooser)

    def __loadSettings(self):
        with open(os.path.join(os.getcwd(), "settings.json"), 'r', encoding='utf8') as jsonFile:
            self.settings = json.loads("".join(jsonFile.readlines()))

    def __setUIComponents(self):
        self.setWindowTitle("OSM Downloader v1.2.")
        # self.listWidget_StatusLog.addItem(QListWidgetItem("text"))
        self.groupBox_Progress.hide()

        self.plainTextEdit_CountriesForSpecificCountriesField.setEnabled(False)
        self.plainTextEdit_SpecificRegionCountriesField.setEnabled(False)
        self.plainTextEdit_CitiesForSpecificCountriesField.setEnabled(False)

        self.toolButton_ImportCitiesFromFile.setEnabled(False)

        self.pushButton_LoadCountries.setCheckable(True)
        self.pushButton_LoadRegions.setCheckable(True)
        self.pushButton_LoadCities.setCheckable(True)

        self.lineEdit_CountryFrom.setEnabled(False)
        self.lineEdit_CountryFrom.setMaxLength(2)

        self.lineEdit_CountryOnly.setEnabled(False)
        self.lineEdit_CountryOnly.setMaxLength(2)

        self.lineEdit_CountryTo.setEnabled(False)
        self.lineEdit_CountryTo.setMaxLength(2)

        self.radioButton_LoadCitiesWithPolygons.setChecked(True)

        self.initAdminLevelComboBox()
        self.initOsmStructureTree()

        self.statusBar.showMessage(f'''Data folder: "{self.settings["path_osm_data"]}"\t\t\t\tLog folder: "{self.settings['path_osm_log']}"''', 10000)

    def __init__(self):
        super(OsmDownloaderWindow, self).__init__()
        self.downloadCitiesWorker = Worker(self.__DownloadCities)
        self.downloadRegionsWorker = Worker(self.__DownloadRegions)
        self.downloadCountriesWorker = Worker(self.__DownloadCountries)
        loadUi("./ui/osm_downloader_window.ui", self)

        self.downloader = OsmDownloader()
        self.threadPool = QThreadPool()

        self.downloadCitiesWorker.signals.log.connect(self.log)
        self.downloadRegionsWorker.signals.log.connect(self.log)
        self.downloadCountriesWorker.signals.log.connect(self.log)

        self.downloader.threadPauseSignal.connect(self.downloadCitiesWorker.pause)
        self.downloader.threadPlaySignal.connect(self.downloadCitiesWorker.play)

        self.downloader.writer.writerLogSignal.connect(self.writerLog)
        self.downloader.logSignal.connect(self.log)
        self.downloader.doneSignal.connect(self.done)
        self.downloader.downloaderChooserSignal.connect(self.openChooser)
        self.downloader.updateTreeSignal.connect(self.treeWidget_OsmStructure.repaint)

        self.__loadSettings()

        self.__setUIComponents()
        self.__setUIConnections()
