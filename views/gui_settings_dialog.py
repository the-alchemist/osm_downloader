#!/usr/bin/python3

import os
import json

import requests
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import *
from PyQt5.uic import loadUi
from PyQt5.QtCore import pyqtSlot


class OsmSettingsDialog(QDialog):

    def __ui_stubs(self):
        self.Form = QWidget()
        self.groupBoxDataFolderPath = QGroupBox()
        self.toolButton_SelectDataPath = QToolButton()
        self.lineEdit_DataFolderPath = QLineEdit()
        self.groupBoxLogPath = QGroupBox()
        self.toolButton_SelectLogPath = QToolButton()
        self.lineEdit_LogPath = QLineEdit()
        self.groupBoxEndpoint = QGroupBox()
        self.label_EndpointStatus = QLabel()
        self.pushButton_CheckEndpoint = QPushButton()
        self.lineEdit_EndpointUrl = QLineEdit()
        self.label_CheckEndpointsLinks = QLabel()
        self.pushButtonSaveSettings = QPushButton()
        raise AssertionError("This should never be called")

    settings = {
        "endpoint": "",
        "path_osm_data": "",
        "path_osm_log": ""
    }

    def loadSettings(self):
        with open(os.path.join(os.getcwd(), "settings.json"), 'r', encoding='utf8') as jsonfile:
            self.settings = json.loads("".join(jsonfile.readlines()))

    def initSettings(self):
        if self.settings["endpoint"]:
            self.lineEdit_EndpointUrl.setText(self.settings["endpoint"])

    def showErrorMessage(self, message):
        errorMessageBox = QMessageBox()
        errorMessageBox.setIcon(QMessageBox.Critical)
        errorMessageBox.setText(message)
        errorMessageBox.setWindowTitle("Error!")
        errorMessageBox.setStandardButtons(QMessageBox.Ok)
        errorMessageBox.exec_()

    # region Slots

    def onSelectDataPath(self):
        dialog = QFileDialog()
        dialog.setFileMode(QFileDialog.DirectoryOnly)
        filePath = dialog.getExistingDirectory(self, 'Select data directory')
        self.lineEdit_DataFolderPath.setText(filePath.replace("/", "\\"))

    def onSelectLogPath(self):
        dialog = QFileDialog()
        dialog.setFileMode(QFileDialog.DirectoryOnly)
        filePath = dialog.getExistingDirectory(self, 'Select log directory')
        self.lineEdit_LogPath.setText(filePath.replace("/", "\\"))

    def onSaveSettings(self):
        try:
            self.settings["path_osm_data"] = self.lineEdit_DataFolderPath.text()
            self.settings["path_osm_log"] = self.lineEdit_LogPath.text()
            self.settings["endpoint"] = self.lineEdit_EndpointUrl.text()
            with open(os.path.join(os.getcwd(), "settings.json"), 'w', encoding='utf8') as jsonfile:
                json_settings = json.dumps(
                    self.settings, indent=4, ensure_ascii=False, sort_keys=True, default=str)
                jsonfile.write(json_settings.replace("\n", ''))
            self.close()
        except Exception as ex:
            self.showErrorMessage(f"Can't write settings!\nReason\n{ex}")

    def onCheckEndpoint(self):
        try:
            requestEndpoint = requests.get(f"{self.lineEdit_EndpointUrl.text()}?data=[out:json];node(107775);out ids;", timeout=3)
            if requestEndpoint.status_code in [200]:
                self.label_EndpointStatus.setText(f"[{requestEndpoint.status_code}] ON")
                self.label_EndpointStatus.setStyleSheet("color:green;")
            else:
                self.label_EndpointStatus.setText(f"[{requestEndpoint.status_code}] OFF")
                self.label_EndpointStatus.setStyleSheet("color:red;")
        except Exception as ex:
            self.label_EndpointStatus.setText(f"ERROR")
            self.label_EndpointStatus.setStyleSheet("color:red;")

    # endregion

    # region Initialization

    def __setConnections(self):
        self.pushButtonSaveSettings.clicked.connect(self.onSaveSettings)
        self.pushButton_CheckEndpoint.clicked.connect(self.onCheckEndpoint)
        self.toolButton_SelectDataPath.clicked.connect(self.onSelectDataPath)
        self.toolButton_SelectLogPath.clicked.connect(self.onSelectLogPath)

    def __setComponents(self):
        self.loadSettings()
        self.initSettings()
        self.setWindowTitle("Settings")

    # endregion

    def __init__(self, parent=None):
        super(OsmSettingsDialog, self).__init__(parent)
        loadUi("./ui/osm_settings_dialog.ui", self)
        self.__setComponents()
        self.__setConnections()
