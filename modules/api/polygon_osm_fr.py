import requests

from modules.api.base import BaseAPIPolygon


class PolygonOsmFrAPI(BaseAPIPolygon):
    def getPolygon(self, id):
        response = requests.get(
            "http://polygons.openstreetmap.fr/get_geojson.py?id={0}".format(id))
        if "None" in response.text:
            return []
        else:
            response_json = response.json()
            return response_json["geometries"][0]