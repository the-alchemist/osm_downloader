import json
import time

import requests
import overpass
from PyQt5.QtCore import pyqtSignal, QObject

from iso3166 import countries

class GeoObject:

    nodes = []
    relations = []
    ways = []


class OverpassAPI(QObject):
    apiChooserSignal = pyqtSignal(str)

    def geoObjectIsValid(self, geoObject):
        if "lat" in geoObject and "lon" in geoObject:
            return True
        return False

    def getCountryByCode(self, code):
        geoCountry = self.api.get(query=f'''relation["admin_level"="2"]["ISO3166-1"="{code}"];''', responseformat="json")
        if geoCountry is not None:
            if len(geoCountry["elements"]) > 0:
                return geoCountry["elements"][0]

    def getCountries(self):
        countries = self.api.get(query=f'''relation["admin_level"="2"];''', responseformat="json")
        return countries

    def getRegionsInCountry(self, code):
        regions = self.api.get(query=f'''relation["ISO3166-2"~"^{code}-"];''', responseformat="json")
        return regions["elements"]

    def getAllCities(self):
        allCities = []
        for level in range(2, 12):
            allCities.append(
                {
                    "admin_level": level,
                    "cities": self.getCities(level)
                }
            )
        return allCities

    def getCities(self, adminLevel):
        citiesOfLevel = []

        cities = self.api.get(query=f'''node[place~"^(city|town)"]["admin_level"="{adminLevel}"]''', responseformat="json")

        citiesOfLevel.append({
            "admin_level": adminLevel,
            "cities": cities
        })
        return citiesOfLevel

    def getCity(self, countryCode, cityTitle):

        city = False

        # Составляем словарик стран и подходящие запросы
        dictionaryCountriesQueryTemplates = {
            "relation": {
                "default": f'''relation["name"="{cityTitle}"][type~"^(boundary|multipolygon)"];'''
            },
            "node": {
                "default": f'''node["name"="{cityTitle}"][place~"^(city|town)"];''',
            }

        }

        cityNode = self.api.get(query=dictionaryCountriesQueryTemplates["node"]["default"], responseformat="json")["elements"]
        cityRelation = self.api.get(query=dictionaryCountriesQueryTemplates["relation"]["default"], responseformat="json")["elements"]

        for elementNode, elementRelation in zip(cityNode, cityRelation):
            if countryCode in json.dumps(elementNode) and cityTitle in json.dumps(elementNode):
                city = cityNode
            if countryCode in json.dumps(elementRelation) and cityTitle in json.dumps(elementRelation):
                city = cityRelation

        for element in city:
            if countryCode in json.dumps(element) and cityTitle in json.dumps(element):
                city = element

        if self.geoObjectIsValid(city):
            return city
        else:
            if "lat" in json.dumps(cityNode) and "lon" in json.dumps(cityNode):
                city["lat"] = cityNode[0]["lat"]
                city["lon"] = cityNode[0]["lon"]
                return city


    def getCityPolygons(self, city):
        cityBoundaries = self.api.get(query=f'''({city["type"]}({city["id"]});<<;)->.a;''', responseformat="json")
        time.sleep(1.01)
        relations = cityBoundaries["elements"]
        if len(relations) > 1:
            relationPolygons = []
            for relation in relations:
                if "admin_level" not in relation["tags"]:
                    relations.remove(relation)
                    continue
                elif int(relation["tags"]["admin_level"]) < 3:
                    relations.remove(relation)
                    continue
                elif "note" in relation["tags"] and "excl" in relation["tags"]["note"]:
                    relations.remove(relation)
                    continue
            for relation in relations:
                relationPolygons.append({"id": relation["id"], "title": relation["tags"]["name"], "geojson": self.getPolygon(relation["id"])})
            return relationPolygons
        else:
            city["geojson"] = self.getPolygon(city)
            return city["geojson"]

    def __getElement(self, type, id):
        element = False
        if id and type:
            element = self.api.get(query=f'''{type}({id})''', responseformat="json")
        return element

    def getWay(self, osmId):
        return self.__getElement("way", osmId)

    def getNode(self, osmId):
        return self.__getElement("node", osmId)

    def getRelation(self, osmId):
        return self.__getElement("rel", osmId)

    def getGeoObject(self, osmId):
        if osmId:
            geoObject = self.api.get(query=f'''relation({id});''', responseformat="json")
            if geoObject:
                geoObject["geojson"] = self.getPolygon(osmId)
                geoObject["namedetails"] = geoObject["elements"][0].pop("tags")
                geoObject["osm_id"] = geoObject["elements"][0].pop("id")

                return geoObject

    def getRegionRelations(self, geoObject):
        relations = []
        for relation in geoObject["elements"][0]["members"]:
            if relation["role"] == "subarea" or relation["type"] == "relation" or (
                    relation["role"] == "admin_centre" and relation["type"] == "node"):
                relations.append(relation)
        return relations

    def getPolygon(self, city):
        response = requests.get(f'''http://polygons.openstreetmap.fr/get_geojson.py?id={city["id"]}''')
        if "None" in response.text:
            yandexCSRFToken = "e017823d1b65b04a48d769b69cf7260835760bc3:1526458773313"
            yandexResponse = requests.get(f'''https://yandex.ru/maps/api/search?csrfToken={yandexCSRFToken}&text={city["tags"]["name"]}&lang=en_US''').json()

            if "error" in yandexResponse:
                #всё, тут уже ничего не придумаешь -_-
                return []
            return yandexResponse
        else:
            responseJson = response.json()
            return responseJson["geometries"][0]

    def chooseCities(self, cities):
        self.apiChooserSignal.emit(cities)

    def __init__(self, endpoint="https://overpass.kumi.systems/api/interpreter"):
        super().__init__()
        self.api = overpass.API(endpoint=endpoint)
