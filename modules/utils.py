#!/usr/bin/python3

import re
import os
import json
import sys
import shutil
from datetime import time, datetime

from PyQt5.QtCore import QObject, pyqtSignal
from iso3166 import countries

from modules.api.overpass import OverpassAPI


# OSM_DATA_DIR_NAME = "\\osm_data_2\\"
# OSM_LOG_DIR_NAME = "\\osm_log_2\\"

def getAdminLevelType(level):
    return {
        "0": "Continent",
        "1": "Empire",
        "2": "Country",
        "3": "MacroRegion",
        "4": "Region",
        "5": "Macrocounty",
        "6": "County",
        "7": "MetroArea",
        "8": "Locality",
        "9": "Macrohood",
        "10": "Neighbourhood",
        "11": "Microhood",
        "12": "Venue",
        "": "Island"
    }[level]


def clarifyString(string):
    # lower_string = string.lower()
    noSpecialCharsString = re.sub(r"[?|$|.|,|!|\\|/]", r'', string)
    # english_only_str = re.sub(r'[^a-zA-Z0-9 ]',r'', no_special_chars_string)
    whitespacesToUnderscores = re.sub(r'[ ]', r'_', noSpecialCharsString)
    return whitespacesToUnderscores


def getCountryCodeByName(countryName):
    if len(countryName) > 2:
        for country in countries:
            if countryName in country.name:
                return country.alpha2


class GeoObjectWriter(QObject):
    writerLogSignal = pyqtSignal(str)

    settings = {
        "endpoint": "",
        "path_osm_data": "",
        "path_osm_log": ""
    }

    def writerLog(self, text):
        self.writerLogSignal.emit(text)

    def __writeGeoObject(self, path, filename, geoObject):
        with open(os.path.join(self.osmDataDirectory, filename), 'w', encoding='utf8') as jsonFile:
            jsonString = json.dumps(geoObject, indent=4, ensure_ascii=False, sort_keys=True, default=str)
            jsonFile.write(jsonString.replace("\n", ''))


    def __loadSettings(self):
        with open(os.path.join(os.getcwd(), "settings.json"), 'r', encoding='utf8') as jsonfile:
            self.settings = json.loads("".join(jsonfile.readlines()))

    def __init__(self):
        super().__init__()
        self.__loadSettings()
        self.osmDataDirectory = self.settings["path_osm_data"]
        self.osmLogDirectory = self.settings["path_osm_log"]

    def makeGeoObject(self, city, polygon):

        cityObject = {
            "namedetails": city["tags"],
            "osm_id": city["id"],
            "lat": city["lat"],
            "lon": city["lon"],
            "geojson": polygon
        }

        if "admin_level" not in city["tags"]:
            cityObject["namedetails"]["admin_level"] = "8"

        return cityObject

    def writeFile(self, geoObject, status="default"):

        makeFolderIfNotExists(self.osmDataDirectory)

        if geoObject:

            osmCountriesGeoObjectDirectoryPath = f"{self.osmDataDirectory}\\OSM_COUNTRIES\\"
            osmRegionsGeoObjectDirectoryPath = f"{self.osmDataDirectory}\\OSM_REGIONS\\{getAdminLevelType(geoObject['namedetails']['admin_level'])}\\"
            osmCitiesGeoObjectDirectoryPath = f"{self.osmDataDirectory}\\OSM_CITIES\\"

            geoObjectOsmId = geoObject["osm_id"]
            geoObjectClarifiedName = clarifyString(geoObject["namedetails"]["name"])
            geoObjectAdminLevelType = getAdminLevelType(geoObject["namedetails"]["admin_level"])

            filename = f'{geoObjectOsmId}_{geoObjectClarifiedName}_{geoObjectAdminLevelType}.json'

            makeFolderIfNotExists(self.osmDataDirectory)

            # if file is already loaded
            if os.path.isfile(os.path.join(self.osmDataDirectory, filename)):
                self.writerLog(f"{geoObjectAdminLevelType} {geoObjectClarifiedName} (ID {geoObjectOsmId}) is already loaded, skipping...")
                return False

            ## Statuses

            if status == "countries":
                makeFolderIfNotExists(osmCountriesGeoObjectDirectoryPath)
                self.__writeGeoObject(osmCountriesGeoObjectDirectoryPath, filename, geoObject)
                self.writerLog(f"[COUNTRY] {geoObjectAdminLevelType} {geoObjectClarifiedName} (ID {geoObjectOsmId}) successfully loaded!")
                return True

            if status == "regions":
                makeFolderIfNotExists(osmRegionsGeoObjectDirectoryPath)
                self.__writeGeoObject(osmRegionsGeoObjectDirectoryPath, filename, geoObject)
                self.writerLog(f"[REGION] {geoObjectAdminLevelType} {geoObjectClarifiedName} (ID {geoObjectOsmId}) successfully loaded!")
                return True

            if status == "cities":
                makeFolderIfNotExists(osmCitiesGeoObjectDirectoryPath)
                self.__writeGeoObject(osmCitiesGeoObjectDirectoryPath, filename, geoObject)
                self.writerLog(f"[CITY] {geoObjectAdminLevelType} {geoObjectClarifiedName} (ID {geoObjectOsmId}) successfully loaded!")
                return True

    def writeLog(self, error):
        makeFolderIfNotExists(self.osmLogDirectory)
        filename = "errors.log"
        with open(os.path.join(self.osmLogDirectory, filename), 'a+', encoding='utf8') as outfile:
            outfile.write(f"[{str(datetime.now)}] Error: {error}\n")
            return True


def clearPoints(self, cleanAdminCenters=False):
    for root, directories, filenames in os.walk(self.osmDataDirectory):
        for index, filename in enumerate(filenames):
            file = os.path.join(root, filename)
            print(f"[{index + 1}:{len(filenames)}] Checking {file} ...")
            try:
                with open(file, 'r+', encoding='utf8') as jsonFile:
                    jsonString = jsonFile.read()
                    jsonDictionary = json.loads(jsonString)
                    jsonPolygonType = jsonDictionary["geojson"]["type"]
                    if jsonPolygonType == "Point":
                        if os.path.isfile(file):
                            jsonFile.close()
                            shutil.rmtree(file)
                            print(f"Removing {file} file, type: Point")
                        else:
                            print(f"Error: {file} file not found")
                    if cleanAdminCenters:
                        if int(jsonDictionary["admin_level"]) > 7:
                            for relation in jsonDictionary["relations"]:
                                if "admin_centre" not in relation["role"]:
                                    if os.path.isfile(file):
                                        jsonFile.close()
                                        shutil.rmtree(file)
                                        print("Removing %s file, type: no admin center" % file)
                                    else:
                                        print("Error: %s file not found" % file)
            except Exception:
                continue


def makeFolderIfNotExists(path):
    if not os.path.exists(path):
        os.makedirs(path)
