import sys
import traceback

from PyQt5.QtCore import QRunnable, QObject, pyqtSignal, pyqtSlot, QMutex, QThread


class WorkerSignals(QObject):
    finished = pyqtSignal()
    error = pyqtSignal(tuple)
    result = pyqtSignal(object)
    progress = pyqtSignal(int)
    itemsLength = pyqtSignal(int)
    log = pyqtSignal(str)


mutex = QMutex()


class Worker(QThread):

    def __init__(self, function, *args, **kwargs):
        super(Worker, self).__init__()

        self.function = function
        self.args = args
        self.kwargs = kwargs
        self.signals = WorkerSignals()

        kwargs["progress_callback"] = self.signals.progress

        # kwargs["log_callback"] = self.signals.log

    def run(self):
        try:
            result = self.function(*self.args, **self.kwargs)
        except:
            traceback.print_exc()
            extype, value = sys.exc_info()[:2]
            self.signals.error.emit((extype, value, traceback.format_exc()))
        else:
            self.signals.result.emit(result)
        finally:
            self.signals.finished.emit()

    def stop(self):
        self.terminate()
        self.wait()
        self.signals.log.emit("\nOperation was cancelled by user.\n")

    def pause(self):
        mutex.lock()

    def play(self):
        mutex.unlock()
