#!/usr/bin/python3
import json
import os

from iso3166 import countries, _CountryLookup

import sys

from modules.utils import GeoObjectWriter, clarifyString, getCountryCodeByName

from modules.api.overpass import OverpassAPI
from PyQt5.QtCore import *

mutex = QMutex()

class OsmDownloader(QObject):
    TIMEOUT_SECS = 50
    PAUSE_BETWEEN_REQUESTS = 1.01000

    doneSignal = pyqtSignal()
    logSignal = pyqtSignal(str)
    itemsLengthSignal = pyqtSignal(int)

    threadPauseSignal = pyqtSignal()
    threadPlaySignal = pyqtSignal()

    updateTreeSignal = pyqtSignal()

    downloaderChooserSignal = pyqtSignal(str)

    ignoredCountries = ['AX', 'AS', 'AQ', 'AW', 'BQ', 'BV', 'CX', 'UM']

    def __getResponseFromChooser(self, citiesUpdated):
        self.response = True
        self.citiesUpdated = citiesUpdated

    def getEndpoint(self):
        with open(os.path.join(os.getcwd(), "settings.json"), 'r', encoding='utf8') as jsonfile:
            self.settings = json.loads("".join(jsonfile.readlines()))
        return self.settings["endpoint"]

    def log(self, text):
        self.logSignal.emit(text)

    def done(self):
        self.doneSignal.emit()

    def LoadGeoObject(self, parent, code, deepestAdminLevel):
        # Check we have parent
        if not parent:
            return
        # Doing our stuff
        geoObject = self.api.getGeoObject(code)
        if geoObject:
            if geoObject != parent:
                parent = geoObject
            else:
                return
            self.writer.writeFile(geoObject)

        geoObjectCountryCode = geoObject["namedetails"]["ISO3166-1"]

        # Checking regions on completeness
        checkRegions = self.api.getRegionsInCountry(geoObjectCountryCode)
        if len(checkRegions) > len(self.api.getRegionRelations(geoObject)):
            self.log(f"[{geoObjectCountryCode}] Regions are broken, trying to download with Overpass...")
            for region in checkRegions:
                if not str(region["id"]) in geoObject["elements"][0]["members"]:
                    geoObject["elements"][0]["members"].append({"ref": region["id"], "role": "subarea", "type": "relation"})
                    self.log(f"Adding {region['id']} to regions list.")
        else:
            self.log("There's no new regions, that's all OSM data... sorry...")
        # If no relations anymore - end of graph
        if len(self.api.getRegionRelations(parent)) == 0:
            self.log("This is the end of graph")
            return
        # Anyway we are recursivly load all of 'em
        for index, relation in enumerate(self.api.getRegionRelations(parent)):
            newCode = relation["ref"]
            try:
                if "--countries" in sys.argv:
                    return
                if int(parent["namedetails"]["admin_level"]) > deepestAdminLevel:
                    self.log("Skipping region, cause it's deeper than user set up")
                    return
                self.LoadGeoObject(parent, newCode)
                self.log(f"[✓] Region [{index + 1} of {len(self.api.getRegionRelations(parent))}]:")
            except Exception as ex:
                if "404" in str(ex):
                    self.log(f"[✗] Region [{index+1} of {len(self.api.getRegionRelations(parent))}]: Error loading region: (ID {relation['ref']}), reason: {ex}, skipping...")
                continue
        return geoObject

    def DownloadCountries(self, countryFrom=None, countryTo=None, oneCountryOnly=None, specificCountries=[]):
        countryList = []
        countryLookup = _CountryLookup()
        for country in countries:
            countryList.append(country)
        if not countryFrom:
            countryFrom = countryList[0].alpha2
        if not countryTo:
            countryTo = countryList[len(countryList) - 1].alpha2
        if oneCountryOnly:
            geoCountry = self.api.getCountryByCode(oneCountryOnly)
            self.log(f"\nCountry {oneCountryOnly} successfully loaded! (only)")
            geoCountryDict = {
                "geojson": self.api.getPolygon(geoCountry["id"]),
                "namedetails": geoCountry["tags"],
                "osm_id": geoCountry["id"]
            }
            self.writer.writeFile(geoCountryDict)
            self.done()
            return
        for index, country in enumerate(countryList):
            if countryFrom and countryTo:
                if index < countryList.index(countryLookup.get(countryFrom)) or index > countryList.index(countryLookup.get(countryTo)):
                    continue
            if country.alpha2 in self.ignoredCountries:
                continue
            else:
                geoCountry = self.api.getCountryByCode(country.alpha2)

            geoCountryDict = {
                "geojson": self.api.getPolygon(geoCountry["id"]),
                "namedetails": geoCountry["tags"],
                "osm_id": geoCountry["id"]
            }

            self.log(f"\nCountry {index + 1} of {len(countries)} - {country.name} [{country.alpha2}]")
            self.writer.writeFile(geoCountryDict)
            self.done()

    def DownloadRegions(self, deepestAdminLevel=12, specifiedCountries=[]):
        countryLookup = _CountryLookup()
        startCountry = countryLookup.get("LV")
        countryList = []
        for country in countries:
            countryList.append(country)
        for index, country in enumerate(countryList):
            if country.alpha2 in self.ignoredCountries:
                continue
            if index < countryList.index(startCountry):
                continue
            geoCountry = self.api.getCountryByCode(country.alpha2)
            self.log(f"\nCountry {index + 1} of {len(countries)} - {country.name} [{country.alpha2}]")
            if geoCountry:
                self.LoadGeoObject(geoCountry, geoCountry["id"], deepestAdminLevel)
            self.done()

    def DownloadCities(self, adminLevel=2, asPoints=False, allLevels=False, specificCities=[]):

        isSpecified = len(specificCities) > 1

        if isSpecified:

            for specifiedCity in specificCities:
                cityCountryName = specifiedCity.split('-')[0].strip()
                cityName = specifiedCity.split('-')[1].strip()
                cityCountryCode = getCountryCodeByName(cityCountryName)

                try:
                    downloadedCity = self.api.getCity(cityCountryCode, cityName)

                    cityPolygon = self.api.getCityPolygons(downloadedCity)

                    cityObject = self.writer.makeGeoObject(downloadedCity, cityPolygon)

                    if len(cityObject["geojson"]) == 0:
                        self.log("There's no polygons... shame on you, OpenStreetMap!")

                    if len(cityObject["geojson"]) > 2:
                        self.threadPauseSignal.emit()
                        self.downloaderChooserSignal.emit(json.dumps(cityObject["boundaries"]))
                        if self.response:
                            self.threadPlaySignal.emit()

                    self.writer.writeFile(cityObject, "cities")
                except Exception as ex:
                    self.log(f"Stupid osm can't load city ({cityCountryCode} - {cityName}), may it have special characters or osm can't find it at all!!!!11113211:\n{ex}")
                    continue

                self.log(f'[✓] Loaded city: ({cityObject["osm_id"]}) {clarifyString(cityObject["namedetails"]["name"])}')
                self.updateTreeSignal.emit()
                self.done()

        else:
            self.log(f"[CITIES MODE]\nDownloading all cities of admin level {adminLevel} (allLevels={allLevels}, asPoints={asPoints})...\n")
            if allLevels:
                cities = self.api.getAllCities()
            else:
                cities = self.api.getCities(adminLevel)

            for cityEnvelope in cities["cities"]:
                for specifiedCity in cityEnvelope["elements"]:
                    try:

                        cityObject = {
                            "namedetails": specifiedCity["tags"],
                            "osm_id": specifiedCity["id"],
                            "lat": specifiedCity["lat"],
                            "lon": specifiedCity["lon"]
                        }

                        if not asPoints:
                            cityPolygons = self.api.getCityPolygons(specifiedCity["id"])
                            cityObject["boundaries"] = []
                            for polygon in cityPolygons:
                                cityObject["boundaries"].append({"geojson": polygon})

                        if "admin_level" not in specifiedCity["tags"]:
                            cityObject["namedetails"]["admin_level"] = "8"

                        self.writer.writeFile(cityObject, "cities")
                        self.log(f'[✓] Loaded city: ({cityObject["osm_id"]}) {clarifyString(cityObject["namedetails"]["name"])}')
                        self.done()

                    except Exception as ex:
                        self.log(f"[✗] Error with ID {specifiedCity['id']}: {ex}")
                        continue

    def __init__(self):
        super().__init__()
        self.api = OverpassAPI(endpoint=self.getEndpoint())
        self.writer = GeoObjectWriter()

