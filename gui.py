#!/usr/bin/python3

import sys

from PyQt5.QtWidgets import *

from views.gui_downloader_window import OsmDownloaderWindow


def catchExceptions(exception, value, traceback):
    print(f"An exception was throwed: {exception}")
    old_hook(exception, value, traceback)
    sys.exit(1)

old_hook = sys.excepthook
sys.excepthook = catchExceptions

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = OsmDownloaderWindow()
    window.show()
    app.exec_()
